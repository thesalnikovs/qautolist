import chalk from "chalk";
import GraphqlGatewayService from "../services/graphql-gateway-service";

class SessionDataHelpers {
  parseSessionIdFromApiResponse(response) {
    const setCookieHeader = response.header["set-cookie"];
    const sessionId = setCookieHeader.filter((cookie) =>
      cookie.startsWith("sessionid")
    )[0];

    const startIndex = sessionId.indexOf("sessionid=") + 10;
    const endIndex = startIndex + 32;
    const sessionIdValue = sessionId.slice(startIndex, endIndex);

    return sessionIdValue;
  }

  async getSessionIdFromBrowserContext() {
    return (await browser.getCookies("sessionid"))[0].value;
  }

  async logSiteUserId() {
    const sessionId = await this.getSessionIdFromBrowserContext();
    const siteUserId = await GraphqlGatewayService.getCurrentSiteUserId(
      sessionId
    );

    console.log(
      chalk.yellowBright(`----------| site_user_id: ${siteUserId} |----------`)
    );
  }

  async getTzacid() {
    const cookies = await browser.getCookies("_tzacid");

    return cookies[0].value;
  }

  async setTzacid(value) {
    await browser.deleteCookie("_tzacid");
    await browser.setCookies([
      {
        name: "_tzacid",
        value,
      },
    ]);
    await browser.pause(500); // eslint-disable-line wdio/no-pause

    const updatedCookie = await this.getTzacid();

    if (updatedCookie != value) {
      await this.setTzacid(value);
    }
  }
}

export default new SessionDataHelpers();
