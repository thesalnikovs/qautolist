import HomePage from "../pages/home-page";

class HomepageHelpers {
  async checkTileHref(vertical, expectedHref) {
    const tile = await HomePage.btnHomePageTile(vertical);
    await expect(tile).toHaveHrefContaining(expectedHref);
  }

  async verifyPhoneNumber() {
    const phoneNumber = await HomePage.btnPhoneNumbHeader.getText();
    expect(phoneNumber).toEqual("1.888.255.4364");
  }

  async verifyPhoneNumberFooter() {
    const phoneNumber = await HomePage.btnPhoneNumbFooter.getText();
    expect(phoneNumber).toEqual("1.888.255.4364");
  }
}

export default new HomepageHelpers();
