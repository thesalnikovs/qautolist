import HomePage from '../../pages/home-page.js';
import { FRONT_SITEMAP } from '../../constants/sitemap';

describe('Autolist: Home Page Landing', () => {
    beforeEach('Open Home Page in Desktop', async () => {
        await HomePage.open(DESKTOP);
    });

    it('FA0001 Should verify clicking "Home" button ', async () => {
        await HomePage.clickTab('home');
        await browser.waitForUrlToContain('autolist.com', TIMEOUTS.MEDIUM);
    });

    it('FA0002 Should verify clicking "Search" button ', async () => {
        await HomePage.clickTab('search');
        await browser.waitForUrlToContain(
            FRONT_SITEMAP.SEARCH,
            TIMEOUTS.MEDIUM,
        );
    });

    it('FA0003 Should verify clicking "Saved searches" button ', async () => {
        await HomePage.clickTab('saved-searches');
        await browser.waitForUrlToContain(
            FRONT_SITEMAP.SAVED_SEARCHES,
            TIMEOUTS.MEDIUM,
        );
    });

    it('FA0004 Should verify clicking "Favorites button ', async () => {
        await HomePage.clickTab('favorites');
        await browser.waitForUrlToContain(
            FRONT_SITEMAP.FAVORITES,
            TIMEOUTS.MEDIUM,
        );
    });
});
