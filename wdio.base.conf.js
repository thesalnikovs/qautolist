/* eslint-disable */
import { promises as fs } from 'fs';
import { Key } from 'webdriverio';
import { expect as chaiExpect } from 'chai';
import request from 'supertest';

import { baseUrl } from './constants/urls';

import { REQUEST_HEADERS } from './constants/common-request-headers';
import { GENERIC_TIMEOUTS } from './constants/timeouts';
import { URLS_TO_BLOCK } from './constants/urls-to-block';
import { DESKTOP, MOBILE } from './constants/window-sizes';

const { ENV } = process.env;
const customRetries = parseInt(process.env.RETRIES) || 0;
const userAgent = process.env.USER_AGENT || 'Zebra Bot/1.0';

globalThis.TIMEOUTS = GENERIC_TIMEOUTS;
globalThis.DESKTOP = DESKTOP;
globalThis.MOBILE = MOBILE;
globalThis.chaiExpect = chaiExpect;
globalThis.request = request;
globalThis.KEYS = Key;
globalThis.REQUEST_HEADERS = REQUEST_HEADERS;

globalThis.isEphemeral = ENV === 'ephemeral';
globalThis.isStaging = ENV === 'staging';
globalThis.isProduction = ENV === 'production';

let networkLogs;

const chromeArgs = [
    // The following commands are needed to resolve the "cannot activate web view" error ---------↓
    '--no-sandbox',
    '--enable-logging',
    '--v=1',
    '--disable-popup-blocking',
    '--disable-notifications',
    '--disable-infobars', // https://stackoverflow.com/a/43840128/1689770
    '--disable-dev-shm-usage', // https://stackoverflow.com/a/50725918/1689770
    '--disable-browser-side-navigation', // https://stackoverflow.com/a/49123152/1689770
    '--disable-gpu', // https://stackoverflow.com/questions/51959986/how-to-solve-selenium-chromedriver-timed-out-receiving-message-from-renderer-exc
    // "--disable-features=VizDisplayCompositor", // https://stackoverflow.com/a/55371396/491553
    // source: https://stackoverflow.com/questions/54297559/getting-cannot-activate-web-view ---------↑
    `--user-agent=${userAgent}`,
];

if (process.env.GUI !== 'true') {
    chromeArgs.push('--headless');
}

const capabilities = [
    {
        browserName: 'chrome',
        'goog:chromeOptions': {
            args: chromeArgs,
        },
        acceptInsecureCerts: true,
    },
];

exports.config = {
    services: ['devtools'],
    /*
  ==================
  Specify Test Files
  ==================
  */
    specs: ['./specs/**/*.js'],
    suites: {
        front: ['./specs/landing-page/*.js'],
    },
    /*
  ============
  Capabilities
  ============
  */
    maxInstances: 5,
    capabilities: capabilities,
    /*
  ===================
  Test Configurations
  ===================
  */
    logLevel: 'warn',
    /*
  If you only want to run your tests until a specific amount of tests have failed use
  bail (default is 0 - don't bail, run all tests).
  */
    bail: 0,
    /*
  Set a base URL in order to shorten url command calls. If your `url` parameter starts
  with `/`, the base url gets prepended, not including the path portion of your baseUrl.
  If your `url` parameter starts without a scheme or `/` (like `some/path`), the base url
  gets prepended directly.
  */
    baseUrl: baseUrl,
    /*
  waitForTimeout - default timeout for all waitFor* commands.
  connectionRetryTimeout - default timeout in ms for request if browser driver or grid doesn't send response
  connectionRetryCount - default request retries count
  */
    waitforTimeout: 10000,
    connectionRetryTimeout: 120000,
    connectionRetryCount: 3,
    /*
  Test Runner Configuration
  Framework you want to run your specs with: https://webdriver.io/docs/frameworks
  Make sure you have the wdio adapter package for the specific framework installed before running any tests.
  specFileRetries - the number of times to retry the entire specfile when it fails as a whole
  specFileRetriesDelay - delay in seconds between the spec file retry attempts
  specFileRetriesDeferred - whether or not retried specfiles should be retried immediately or deferred to the end of the queue
  */
    framework: 'mocha',
    specFileRetries: customRetries,
    specFileRetriesDelay: 0,
    specFileRetriesDeferred: false,
    /*
  Reporters Configuration
  To configure a specific reporter refer to:
  https://webdriver.io/docs/spec-reporter/
  */
    reporters: [
        [
            'spec',
            {
                showPreface: false,
                onlyFailures: false,
                addConsoleLogs: true,
                realtimeReporting: false || process.env.REALTIME_REPORTING,
                symbols: {
                    passed: '[PASS]',
                    failed: '[FAIL]',
                    skipped: '[SKIP]',
                },
            },
        ],
    ],
    mochaOpts: {
        ui: 'bdd',
        timeout: 100000,
        bail: process.env.BAIL === 'false' ? false : true,
    },
    /*
  =====
  Hooks
  =====
  */
    /**
     * Gets executed before test execution begins. At this point you can access to all global
     * variables like `browser`. It is the perfect place to define custom commands.
     * @param {Array.<Object>} capabilities list of capabilities details
     * @param {Array.<String>} specs        List of spec file paths that are to be run
     * @param {Object}         browser      instance of created browser/device session
     */
    before: function (capabilities, specs) {
        /**
         * Waits until the new tab pops up, closes original tab and switches the context
         * @param tabIndex {number} - index of the tab you want to focus on
         * @param numOfTabs {number} optional - expected number of tabs within the browser session
         */
        browser.addCommand(
            'waitAndSwitchToTab',
            async function (tabIndex, numOfTabs = 2) {
                let tabs;

                await browser.waitUntil(async () => {
                    tabs = await browser.getWindowHandles();
                    return tabs.length === numOfTabs;
                });

                for (let i = 0; i < numOfTabs; i++) {
                    if (i == tabIndex) break;
                    await browser.switchToWindow(tabs[i]);
                    await browser.closeWindow();
                }

                await browser.switchToWindow(tabs[tabIndex]);
            },
        );
        browser.addCommand(
            'waitForAnimationToStop',
            async function (timeout = 5000) {
                try {
                    await browser.waitUntil(
                        async () => {
                            const oldCoordinates = await browser.getElementRect(
                                this.elementId,
                            );
                            await browser.pause(250); // eslint-disable-line wdio/no-pause
                            const newCoordinates = await browser.getElementRect(
                                this.elementId,
                            );
                            return (
                                oldCoordinates.x == newCoordinates.x &&
                                oldCoordinates.y == newCoordinates.y &&
                                oldCoordinates.width == newCoordinates.width &&
                                oldCoordinates.height == newCoordinates.height
                            );
                        },
                        {
                            timeout: timeout,
                            timeoutMsg: `Animation is still in progress after ${timeout} ms`,
                            interval: 250,
                        },
                    );
                } catch (err) {
                    if (err.toString().includes('stale')) {
                        return;
                    } else {
                        throw new Error(err.toString());
                    }
                }
            },
            true,
        );
        /**
         * @param {Boolean} noAnimation - if set to true, element will be checked for animation in progress
         */
        browser.addCommand(
            'waitAndClick',
            async function (noAnimation) {
                await this.waitForDisplayed();
                await this.waitForEnabled();
                if (noAnimation) {
                    await this.waitForAnimationToStop();
                }
                await this.click();
            },
            true,
        );

        browser.addCommand(
            'waitAndSetValue',
            async function (value, noAnimation) {
                await this.waitForDisplayed();
                await this.waitForEnabled();
                if (noAnimation) {
                    await this.waitForAnimationToStop();
                }
                await this.clearValue();
                await this.addValue(value);
            },
            true,
        );

        browser.addCommand(
            'waitForUrlToContain',
            async function (value, timeout = 5000) {
                await browser.waitUntil(
                    async function () {
                        const currentUrl = await browser.getUrl();
                        return currentUrl.indexOf(value) > -1;
                    },
                    {
                        timeout: timeout,
                        timeoutMsg: `Timeout error! Url doesn't contain ${value} after ${timeout} ms`,
                        interval: 500,
                    },
                );
            },
        );

        browser.addCommand(
            'waitForUrlToNotContain',
            async function (value, timeout = 5000) {
                await browser.waitUntil(
                    async function () {
                        const currentUrl = await browser.getUrl();
                        return currentUrl.indexOf(value) === -1;
                    },
                    {
                        timeout: timeout,
                        timeoutMsg: `Timeout error! Url still contains ${value} after ${timeout} ms`,
                        interval: 500,
                    },
                );
            },
        );

        browser.addCommand(
            'triggerBlur',
            async function () {
                const isFocused = await this.isFocused();
                if (!isFocused) {
                    await this.waitAndClick();
                }
                await browser.keys(KEYS.Tab);
                await browser.pause(750); // Delay is needed when running headless
            },
            true,
        );
    },
    /**
     * Hook that gets executed before the suite starts
     * @param {Object} suite suite details
     */
    beforeSuite: async function (suite) {
        // https://chromedevtools.github.io/devtools-protocol/tot/Network/#method-setBlockedURLs
        await browser.cdp('Network', 'enable');
        await browser.cdp('Network', 'setBlockedURLs', {
            urls: URLS_TO_BLOCK,
        });

        networkLogs = await browser.mock('**/events/'); // This initializes the process of capturing network logs for the suite
    },
    /**
     * Gets executed before initializing the webdriver session and test framework. It allows you
     * to manipulate configurations depending on the capability or spec.
     * @param {object} config wdio configuration object
     * @param {Array.<Object>} capabilities list of capabilities details
     * @param {Array.<String>} specs List of spec file paths that are to be run
     */
    beforeSession: function (config, capabilities, specs) {
        // Update capabilities based on a filename
        const { width, height } = MOBILE;
        if (specs[0].includes('mobile')) {
            capabilities['goog:chromeOptions'].args.push(
                `--window-size=${width},${height}`,
            );
        }
    },
    /**
     * Function to be executed before a test (in Mocha/Jasmine only)
     * @param {Object} test    test object
     * @param {Object} context scope object the test was executed with
     */
    beforeTest: function (test, context) {
        if (ENV === 'production') {
            browser.setCookies({ name: 'channelid', value: 'v00h11' });
        }
    },
    /**
     * Function to be executed after a test (in Mocha/Jasmine only)
     * @param {Object}  test             test object
     * @param {Object}  context          scope object the test was executed with
     * @param {Error}   result.error     error object in case the test fails, otherwise `undefined`
     * @param {Any}     result.result    return object of test function
     * @param {Number}  result.duration  duration of test
     * @param {Boolean} result.passed    true if test has passed, otherwise false
     * @param {Object}  result.retries   informations to spec related retries, e.g. `{ attempts: 0, limit: 0 }`
     */
    afterTest: async function (
        test,
        context,
        { error, result, duration, passed, retries },
    ) {
        if (!passed) {
            await this.saveScreenshot(test);
        }
    },
    /**
     * Hook that gets executed after the suite has ended
     * @param {Object} suite suite details
     */
    afterSuite: async function (suite) {
        // This captures the network logs from the suite
        let logTitle = suite.title.replace(/[\W+_]+/g, '_');
        if (logTitle.slice(-1) == '_') logTitle = logTitle.slice(0, -1);
        let filtered = [];

        for (let x = 0; x < networkLogs.calls.length; x++) {
            if (networkLogs.calls[x].hasOwnProperty('postData')) {
                filtered.push(JSON.parse(networkLogs.calls[x].postData));
            }
        }

        filtered = filtered.flat();

        if (filtered.length > 0) {
            await fs.writeFile(
                `network_logs/${logTitle}.json`,
                JSON.stringify(filtered),
            );
            await dataParse.buildEvent(filtered, logTitle);
        }
    },
    /**
     * Gets executed after all workers got shut down and the process is about to exit. An error
     * thrown in the onComplete hook will result in the test run failing.
     * @param {Object} exitCode 0 - success, 1 - fail
     * @param {Object} config wdio configuration object
     * @param {Array.<Object>} capabilities list of capabilities details
     * @param {<Object>} results object containing test results
     */
    onComplete: async function (exitCode) {
        return exitCode;
    },
    /**
     * Records a screenshot of a failed test
     * @param {Object} test test object from spec
     */
    saveScreenshot: async function (test) {
        const filepath = test.parent.replace(/\s/g, '');
        await browser.saveScreenshot(
            `reports/screenshots/${filepath}-{${browser.sessionId}}.png`,
        );
    },
};
