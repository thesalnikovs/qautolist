export const FRONT_SITEMAP = {
    SEARCH: '/listings',
    SAVED_SEARCHES: '/user/saved-searches',
    FAVORITES: '/user/favorites',
};
