const buildBaseUrl = () => {
    switch (process.env.ENV) {
        case 'staging':
            return 'https://staging.autolist.dev';
        case 'prod':
            return 'https://www.autolist.com';
        default:
            throw new Error('Environment is not recognized');
    }
};

export const baseUrl = buildBaseUrl();

/**
 * @param {string} host - points to a specific application/service
 * @param {boolean} switchEphemeralToDev - (optional) if set to true, constructor will return dev URL for epehemeral
 */
export const buildUrl = (host) => {
    switch (process.env.ENV) {
        case 'staging':
            return `https://${host}.staging.autolist.dev`;
        case 'prod':
            return `https://${host}.production.autolist.com`;
        default:
            throw new Error('Environment is not recognized');
    }
};
