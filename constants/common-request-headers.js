export const REQUEST_HEADERS = {
    'Content-Type': 'application/json',
    'User-Agent': 'Autolist Bot/1.0',
    'X-Zebra-Client-Identifier': 'QAutolist',
};
