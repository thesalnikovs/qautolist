/**
 * main page object containing all methods, selectors and functionality
 * that is shared across all page objects
 */
export default class BasePage {
  /**
   * Opens a sub page of the page
   * @param path {string} path of the sub page (e.g. /path/to/page.html)
   */
  async open(path, windowSize) {
    // Size options can be found in the lib/window-sizes.js file
    await browser.setWindowSize(windowSize.width, windowSize.height);
    await browser.url(path);
  }
}
