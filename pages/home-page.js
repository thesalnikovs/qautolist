import BasePage from "./base-page";
import { baseUrl } from "../constants/urls";

class HomePage extends BasePage {
    btnHomePageTab(value) {
        return $(`[data-testid="header-${value}-link"]`);
    }

    get txtHomePageHero() {
        return $('h1.hero-headline');
    }

    get btnLoginMobile() {
        return $('//a[text()="Log-in to my dashboard"]');
    }

    get btnHamburgerMenu() {
        return $('.header-burger');
    }

    get btnLogin() {
        return $('[data-cy="homepage_login"]');
    }

    get btnPhoneNumbHeader() {
        return $(
            "a[class='cta-phone cta-phone-link'] span[class='cta-phone-display']",
        );
    }

    get btnPhoneNumbFooter() {
        return $('.hidden-md-down.cta-phone-display');
    }

    get inpZipcode() {
        return $('[data-cy="zipcode-form-control"]');
    }

    get btnSubmitZipcode() {
        return $('[data-cy="zipcode-submit-button"]');
    }

    get txtHeadline() {
        return $('h1.hero-headline');
    }

    get txtModalTitle() {
        return $('#product-cta-modal-title');
    }

    async open(windowSize, urlSlug = "") {
        await super.open(`${baseUrl}${urlSlug}`, windowSize);
      }

    async clickTab(value) {
        await this.btnHomePageTab(value).waitAndClick(true);
    }

    async showAllTiles() {
        await this.clickTile('more');
        await this.txtModalTitle.waitForDisplayed();
    }

    async enterAutoFunnel(windowSize) {
        await this.open(windowSize);
        await this.clickTile('auto');
        await browser.waitForUrlToContain(
            AUTO_CLIENT_SITEMAP.CAR_BASE_PATH,
            TIMEOUTS.LONG,
        );
    }

    async enterHomeFunnel(windowSize) {
        await this.open(windowSize);
        await this.clickTile('home');
        await browser.waitForUrlToContain(
            AUTO_CLIENT_SITEMAP.HOME_BASE_PATH,
            TIMEOUTS.LONG,
        );
    }

    async enterBundleFunnel(windowSize) {
        await this.open(windowSize);
        await this.clickTile('auto-home');
        await browser.waitForUrlToContain('/insurance', TIMEOUTS.LONG);
    }

    async openLoginPage() {
        await this.btnLogin.waitAndClick(true);
        await browser.waitForUrlToContain(
            ZFRONT_SITEMAP.LOGIN,
            TIMEOUTS.MEDIUM,
        );
    }

    async openLoginPageMobile() {
        await this.btnHamburgerMenu.waitAndClick();
        await this.btnLoginMobile.waitAndClick(true);
        await browser.waitForUrlToContain(
            ZFRONT_SITEMAP.LOGIN,
            TIMEOUTS.MEDIUM,
        );
    }
}

export default new HomePage();
